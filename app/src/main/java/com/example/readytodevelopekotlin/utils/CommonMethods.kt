package com.example.readytodevelopekotlin.utils

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Build
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentTransaction
import com.example.readytodevelopekotlin.R
import com.example.readytodevelopekotlin.fragments.BaseFragment
import java.lang.Exception




open class CommonMethods{

    companion object{
        private var pgDialog: Dialog? = null
        fun showProgressDialog(mActivity: FragmentActivity) {
            try {
                if (pgDialog != null) {
                    pgDialog!!.hide()
                }
                pgDialog = Dialog(mActivity)
                pgDialog?.let { pgDialog->

                    pgDialog.setCancelable(false)

                    val view: View = mActivity.layoutInflater.inflate(R.layout.loti_loading, null)
                    pgDialog.setContentView(view)

                    if (pgDialog.getWindow() != null)
                        pgDialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

                    pgDialog.show()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        fun hideProgressDialog(){
            pgDialog?.let { it ->
                it.dismiss()
                pgDialog=null
            }
        }

        //Load a fragment to frame layout with animation
        fun callFragment(
            nFragment: BaseFragment,
            view: Int,
            enterAnim: Int,
            exitAnim: Int,
            mActivity: FragmentActivity,
            isBack: Boolean
        ) {
            var enterAnim = enterAnim
            var exitAnim = exitAnim
            val fm = mActivity.supportFragmentManager
            val fragmentTransaction = fm.beginTransaction()
            fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            var enterPopAnim: Int
            var exitPopAnim: Int
            enterPopAnim = R.anim.slide_in_left
            exitPopAnim = R.anim.slide_out_right
            if (enterAnim == 0) {
                enterAnim = R.anim.slide_in_right
            }
            if (exitAnim == 0) {
                exitAnim = R.anim.slide_out_left
            }
            if (enterAnim == R.anim.fade_in) {
                enterPopAnim = R.anim.fade_in
            }
            if (exitAnim == R.anim.fade_out) {
                exitPopAnim = R.anim.fade_out
            }
            if (enterAnim == -1 && exitAnim == -1) {
                enterAnim = 0
                exitAnim = 0
                enterPopAnim = 0
                exitPopAnim = 0
            }
            fragmentTransaction.setCustomAnimations(enterAnim, exitAnim, enterPopAnim, exitPopAnim)
            fragmentTransaction.replace(view, nFragment, nFragment.fragmentTag)
            if (isBack) {
                fragmentTransaction.addToBackStack(nFragment.fragmentTag)
            }
            //        else
//        {
//            if(mActivity.getSupportFragmentManager().getBackStackEntryCount()>0) {
//                mActivity.getSupportFragmentManager().popBackStack();
//            }
//        }
            try {
                fragmentTransaction.commitAllowingStateLoss()
            } catch (e: Exception) {
                e.printStackTrace()
                fragmentTransaction.commit()
            }
        }

        fun setupUI(view: View, context: Context?) {

            //Set up touch listener for non-text box views to hide keyboard.
            if (view !is EditText) {
                view.setOnTouchListener { v, event ->
                    hideSoftKeyboard(view, context!!)
                    false
                }
            }

            //If a layout container, iterate over children and seed recursion.
            if (view is ViewGroup) {
                for (i in 0 until view.childCount) {
                    val innerView = view.getChildAt(i)
                    setupUI(innerView, context)
                }
            }
        }

        fun hideSoftKeyboard(v: View, context: Context) {
            try {
                val imm =
                    context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(v.windowToken, 0)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        fun isNetworkAvailable(mContext: Context): Boolean {
            try {

                if(Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
                    val cm =
                        mContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                    val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
                    val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true

                    return isConnected
                }else{
                    return true
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return false
        }
    }
}
