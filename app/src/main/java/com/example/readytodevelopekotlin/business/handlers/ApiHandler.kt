package com.example.readytodevelopekotlin.business.handlers

import com.androidnetworking.common.Priority
import com.example.readytodevelopekotlin.business.interfaces.OnQuizLoadListener
import com.example.readytodevelopekotlin.business.interfaces.OnServerResultNotifier

class ApiHandler {
    companion object{
        fun callGetApi(amount:Int,onQuizLoadListener:OnQuizLoadListener){
            var url:String="https://opentdb.com/api.php?amount=${amount}"
            CallForServer(url, object : OnServerResultNotifier {
                override fun onServerResultNotifier(isError: Boolean, response: String?) {

//                CommonMethods.hideProgressDialog();
                    if (!isError) {
                        onQuizLoadListener.onSuccess(response)
                    } else {
                        onQuizLoadListener.onError(response)
                    }
                }
            }).callForServerGet("quizApi",Priority.LOW)
        }

    }
}