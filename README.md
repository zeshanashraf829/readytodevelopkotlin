# Android Basic App developed in Kotlin With API implementation, ViewBinding and RoomDB

# API
	-For API implementation use Android Networking Library

# Fragmentation
	
	-For adding New Fragment don't forget to extend with BasicFragment which has all required implementation
	-For adding Main Fragments use "flFragmentContainer"
	-For adding Child Fragments use "flFragmentOverlay"

# Packages and Uses

	-Activities => For all activities of the app

	-Business =>is use For API related work including models and interfaces

				Hanlders-> is use for API callings and response handling
				Interface-> For all interfaces of the app which uses callbacks and implementations
				Models-> For all data models of the app which uses serializing using gson and for api responses

	-Fragments => For all Fragments of the app

	-Preferences => For Preferences flow which uses to save small amount of data

	-RoomDB => for Local Database implementations

	-Utils => for other utilities like CommonObjects and CommonMethods


# Owned and Developed by
	# Rana Zeshan Ashraf
	# zeshanashraf829@gmail.com